namespace Program
{
    public class Program
    {
        public int[] TwoSum(int[] nums, int target)
        {
            int[] solved = new int[2];

            for (int first = 0; first < nums.Length - 1; first++)
            {
                for (int second = first + 1; second < nums.Length; second++)
                {
                    if (nums[first] + nums[second] == target)
                    {
                        solved[0] = first;
                        solved[1] = second;
                        return solved;
                    }
                }
            }
            return Array.Empty<int>();

        }

    }

    public class UnitTest1
    {
        [Fact]
        public void GivenCorrectNumbers_WhenTargetSet_ThenPass1()
        {
            //assign
            int[] nums = { 2, 7, 11, 15 };
            int target = 9;
            int[] result = { 0, 1 };

            //act
            Program solution = new Program();

            //assert
            Assert.Equal(result, solution.TwoSum(nums, target));
        }

        [Fact]
        public void GivenCorrectNumbers_WhenTargetSet_ThenPass2()
        {
            //assign
            int[] nums = { 3, 2, 4 };
            int target = 6;
            int[] result = { 1, 2 };

            //act
            Program solution = new Program();

            //assert
            Assert.Equal(result, solution.TwoSum(nums, target));
        }

        [Fact]
        public void GivenCorrectNumbers_WhenTargetSet_ThenPass3()
        {
            //assign
            int[] nums = { 3,3 };
            int target = 6;
            int[] result = { 0, 1 };

            //act
            Program solution = new Program();

            //assert
            Assert.Equal(result, solution.TwoSum(nums, target));
        }

    }
}

